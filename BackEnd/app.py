#Python Libraries
import os.path as p
from flask import Flask, request, jsonify, send_file, render_template

#Flask information
app = Flask(__name__)

#Flask settings
app.secret_key="mySecretKey"
#predefined paths
#app_root = p.dirname(p.abspath(__file__))
#app_route=p.join(app_root, 'ruta/')

#Flask routes
@app.route('/',methods=['GET'])
def Index():
    #
    #code
    #
    return render_template('Index.html')

@app.route('/API/Documents',methods=['GET'])
def getDocuments():
    #
    #code
    #
    return jsonify({'documents':'get information'})

@app.route('/API/Documents',methods=['POST'])
def postDocuments():
    if request.method == 'POST':
        #
        #code
        #
        return jsonify({'message':'post information'})

@app.route('/API/Documents/<string:ID>',methods=['GET'])
def getDocument(ID):
    #
    #code
    #
    return jsonify({'document':'get information'})

@app.route('/API/Documents/<string:ID>',methods=['POST'])
def postDocument(ID):
    if request.method == 'POST':
        #
        #code
        #
        return jsonify({'document':'post information'})

@app.route('/API/Documents/<string:ID>',methods=['PUT'])
def putDocument(ID):
    if request.method == 'PUT':
        #
        #code
        #
        return jsonify({'document':'put information'})

@app.route('/API/Documents/<string:ID>',methods=['DELETE'])
def deleteDocument(ID):
    if request.method == 'DELETE':
        #
        #code
        #
        return jsonify({'document':'delete information'})

@app.route('/API/Documents/Version/<string:ID>',methods=['GET'])
def getVersion(ID):
    #
    #code
    #
    return jsonify({'documents':' get information'})

#starting app
if __name__ == "__main__":
    app.run(port=4000,debug=True)